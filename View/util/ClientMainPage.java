import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import java.net.URL;
import java.util.ResourceBundle;

public class ClientMainPage implements Initializable {

    private static String first_name = Users.getUserFirstName();
    private static String last_name = Users.getUserLastName();

    @FXML
    private Label welcome;

    public void logout(ActionEvent actionEvent) {

        Class class_type = getClass();
        Buttons.logout(actionEvent, class_type);
    }

    public void checkClothesStatus(ActionEvent actionEvent) {
        String path = Controller.getFXMLRoute("CheckClothesStatus");
        Class class_type = getClass();
        Controller.loadForcedFXML(path, class_type);
    }

    public void addClothes(ActionEvent actionEvent) {
        String path = Controller.getFXMLRoute("AddClothes");
        Class class_type = getClass();
        Controller.loadForcedFXML(path, class_type);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        welcome.setText("Welcome, " + last_name + " " + first_name);
    }
}
