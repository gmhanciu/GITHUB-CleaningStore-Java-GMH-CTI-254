import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.converter.IntegerStringConverter;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class AddClothes implements Initializable {

    private String[] types = ClothesTypes.getAllTypes();

    private Integer[] prices = ClothesTypes.getAllPrices();

    @FXML
    private TableView<AddClothesTable> add_clothes_table;

    @FXML
    private TableColumn<AddClothesTable, String> clothes_types;

    @FXML
    private TableColumn<AddClothesTable, Integer> number_of_clothes;

    @FXML
    private TableColumn<AddClothesTable, Integer> price;


    public void goBack(ActionEvent actionEvent) {
        try {
            String route = "ClientMainPage";
            Class class_type = getClass();
            Buttons.goBack(actionEvent, route, class_type);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void completeOrder(ActionEvent actionEvent) {
        int total_price = 0;
        Map<String, Integer> clothes_data = new HashMap<>();
        for (AddClothesTable row_data : add_clothes_table.getItems()) {
            if (number_of_clothes.getCellData(row_data) != 0) {
                clothes_data.put(clothes_types.getCellData(row_data), number_of_clothes.getCellData(row_data));
                total_price += number_of_clothes.getCellData(row_data) * price.getCellData(row_data);
            }
        }
        Receipts.create(Users.getUserID(), total_price, clothes_data);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        clothes_types.setCellValueFactory(new PropertyValueFactory<AddClothesTable, String>("clothesTypes"));
        number_of_clothes.setCellValueFactory(new PropertyValueFactory<AddClothesTable, Integer>("numberOfClothes"));
        price.setCellValueFactory(new PropertyValueFactory<AddClothesTable, Integer>("price"));
        number_of_clothes.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        ObservableList<AddClothesTable> add_clothes_table_data = FXCollections.observableArrayList();
        for (int i = 0; i < types.length; i++) {
            add_clothes_table_data.add(new AddClothesTable(types[i], 0, prices[i]));
        }
        add_clothes_table.setItems(add_clothes_table_data);
    }

    public void editNumarArticole(TableColumn.CellEditEvent<AddClothesTable, Integer> addClothesTableIntegerCellEditEvent) {
        AddClothesTable new_number_of_clothes = add_clothes_table.getSelectionModel().getSelectedItem();
        new_number_of_clothes.setNumberOfClothes(addClothesTableIntegerCellEditEvent.getNewValue());
    }
}
