import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import javax.swing.*;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class CreateAccountFormController implements Initializable {

    private static Parent rootParent;
    private static Stage mainStage = MainPage.getMainStage();

    @FXML
    private TextField username;

    @FXML
    private PasswordField password;

    @FXML
    private TextField email;

    @FXML
    private TextField first_name;

    @FXML
    private TextField last_name;

    private static Connection connection = Controller.getConnection();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if (Controller.getConnection() == null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean checkIfAccountExists(String user, String emailInsert) {
        try {
            PreparedStatement check_for_existing_account = null;
            String check_acc_sql = "SELECT username, email FROM users WHERE username = ? OR email = ?";
            check_for_existing_account = connection.prepareStatement(check_acc_sql);
            check_for_existing_account.setString(1, user);
            check_for_existing_account.setString(2, emailInsert);
            ResultSet resultSet = check_for_existing_account.executeQuery();
            if (resultSet.next()) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static String encryptPassword(String pass) {
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(pass.getBytes());
            //Get the hash's bytes
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            pass = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return pass;
    }

    public void createAccount(ActionEvent actionEvent) {

        try {
            String user = username.getText();
            String pass = password.getText();
            String emailInsert = email.getText();
            String firstname = first_name.getText();
            String lastname = last_name.getText();
            if (user.isEmpty() || pass.isEmpty() || emailInsert.isEmpty() || firstname.isEmpty() || lastname.isEmpty()) {
                JOptionPane.showMessageDialog(null, "Please fill in all the fields!");
            } else {
                if (!checkIfAccountExists(user, emailInsert)) {
                    pass = encryptPassword(pass);
                    PreparedStatement create_account = null;
                    String sql = "INSERT INTO users (username, password, email, first_name, last_name) VALUES (?, ?, ?, ?, ?)";
                    create_account = connection.prepareStatement(sql);
                    create_account.setString(1, user);
                    create_account.setString(2, pass);
                    create_account.setString(3, emailInsert);
                    create_account.setString(4, firstname);
                    create_account.setString(5, lastname);
                    int check_insert = create_account.executeUpdate();
                    if (check_insert != 0) {
                        int input = JOptionPane.showOptionDialog(null, "Account has been successfully created!", "Success!", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
                        if (input == JOptionPane.OK_OPTION || input == JOptionPane.CLOSED_OPTION) {
                            Users.setUserName(user);
                            Users.setUserAccess(Integer.parseInt("2"));
                            Users.setUserFirstName(firstname);
                            Users.setUserLastName(lastname);
                            PreparedStatement get_new_registered_user_id = null;
                            String sql2 = "SELECT id FROM users where username = ? AND password = ?";
                            get_new_registered_user_id = connection.prepareStatement(sql2);
                            get_new_registered_user_id.setString(1, user);
                            get_new_registered_user_id.setString(2, pass);
                            ResultSet rs = get_new_registered_user_id.executeQuery();
                            rs.next();
                            Users.setUserID(rs.getInt("id"));
                            String path = Controller.getFXMLRoute("ClientMainPage");
                            Class class_type = getClass();
                            Controller.loadForcedFXML(path, class_type);

                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Pleaese try again creating an account!");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Username or email is already in use!");

                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void goBack(ActionEvent actionEvent) {

        try {
            String route = "LoginForm";
            Class class_type = getClass();
            Buttons.goBack(actionEvent, route, class_type);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
