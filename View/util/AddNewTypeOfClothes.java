import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import javax.swing.*;

public class AddNewTypeOfClothes {

    @FXML
    private TextField type_name;

    @FXML
    private TextField type_price;

    public void insertNewTypeOfClothes(ActionEvent actionEvent) {
        if (type_name.getText().isEmpty() || type_price.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Please fill in all the fields!");
        } else {
            ClothesTypes.insertType(type_name.getText(), Integer.parseInt(type_price.getText()));
            int input = JOptionPane.showOptionDialog(null, "New type added successfully!", "Success!", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
            if (input == JOptionPane.OK_OPTION || input == JOptionPane.CLOSED_OPTION) {
                String path = Controller.getFXMLRoute("EmployeeMainPage");
                Class class_type = getClass();
                Controller.loadForcedFXML(path, class_type);
            }
        }
    }

    public void goBack(ActionEvent actionEvent) {
        String route = "EmployeeMainPage";
        Class class_type = getClass();
        Buttons.goBack(actionEvent, route, class_type);
    }
}
