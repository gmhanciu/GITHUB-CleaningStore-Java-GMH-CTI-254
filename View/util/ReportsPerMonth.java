import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;

import java.net.URL;
import java.util.*;

public class ReportsPerMonth implements Initializable {

    @FXML
    public TableView<ReportsPerMonthTable> reports_per_month_table;

    @FXML
    public TableColumn<ReportsPerMonthTable, String> clothes_type;

    @FXML
    public TableColumn<ReportsPerMonthTable, Integer> price;

    @FXML
    public TableColumn<ReportsPerMonthTable, Integer> cleaned_clothes_number;

    @FXML
    public TableColumn<ReportsPerMonthTable, Integer> total_cash_earned;

    @FXML
    public TextField filter_by_clothes_type;

    private Map<List<String>, Map<List<Integer>, Map<List<Integer>, List<Integer>>>> reports_per_month = Clothes.getReportsPerMonthData();

    private List<String> clothes_types = new ArrayList<>();
    private List<Integer> prices = new ArrayList<>();
    private List<Integer> cleaned_clothes_numbers = new ArrayList<>();
    private List<Integer> total_cash = new ArrayList<>();

    private ObservableList<ReportsPerMonthTable> reports_per_month_data = FXCollections.observableArrayList();


    public void filterByClothesType(KeyEvent keyEvent) {
        FilteredList<ReportsPerMonthTable> filtered_data = new FilteredList<ReportsPerMonthTable>(reports_per_month_data, p -> true);

        filter_by_clothes_type.textProperty().addListener((observable, old_value, new_value) -> {
            filtered_data.setPredicate(ReportsPerMonthTable ->
            {
                if (new_value == null || new_value.isEmpty()) {
                    return true;
                }

                String lowercase_filter = new_value.toLowerCase();

                //ADD MORE CHECKS IF NEEDED
                if (String.valueOf(ReportsPerMonthTable.getClothesType()).toLowerCase().contains(lowercase_filter)) {
                    return true;
                }

                return false;
            });
        });

        SortedList<ReportsPerMonthTable> sorted_data = new SortedList<>(filtered_data);

        sorted_data.comparatorProperty().bind(reports_per_month_table.comparatorProperty());

        reports_per_month_table.setItems(sorted_data);
    }

    public void goBack(ActionEvent actionEvent) {
        String route = "Reports";
        Class class_type = getClass();
        Buttons.goBack(actionEvent, route, class_type);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        clothes_type.setCellValueFactory(new PropertyValueFactory<ReportsPerMonthTable, String>("clothesType"));
        price.setCellValueFactory(new PropertyValueFactory<ReportsPerMonthTable, Integer>("price"));
        cleaned_clothes_number.setCellValueFactory(new PropertyValueFactory<ReportsPerMonthTable, Integer>("cleanedClothesNumber"));
        total_cash_earned.setCellValueFactory(new PropertyValueFactory<ReportsPerMonthTable, Integer>("totalCashEarned"));

        for (Map.Entry<List<String>, Map<List<Integer>, Map<List<Integer>, List<Integer>>>> names_prices_cleaned_cash : reports_per_month.entrySet()) {
            Map<List<Integer>, Map<List<Integer>, List<Integer>>> price_clothes_number_total_cash = names_prices_cleaned_cash.getValue();
            for (Map.Entry<List<Integer>, Map<List<Integer>, List<Integer>>> prices_cleaned_cash : price_clothes_number_total_cash.entrySet()) {
                Map<List<Integer>, List<Integer>> clothes_number_total_cash = prices_cleaned_cash.getValue();
                for (Map.Entry<List<Integer>, List<Integer>> cleaned_cash : clothes_number_total_cash.entrySet()) {
                    cleaned_clothes_numbers = cleaned_cash.getKey();
                    total_cash = cleaned_cash.getValue();
                }
                prices = prices_cleaned_cash.getKey();
            }
            clothes_types = names_prices_cleaned_cash.getKey();
        }

        for (int i = 0; i < clothes_types.size(); i++) {
            reports_per_month_data.add(new ReportsPerMonthTable(clothes_types.get(i), prices.get(i), cleaned_clothes_numbers.get(i), total_cash.get(i)));
        }
        reports_per_month_table.setItems((reports_per_month_data));
    }
}
