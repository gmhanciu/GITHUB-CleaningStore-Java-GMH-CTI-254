import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import javax.swing.*;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class ChangeClothesStatus implements Initializable {

    private HashMap<Integer, Integer> change_clothes_status_table_data = Clothes.getClothesByStatus(0);

    private ObservableList<ChangeClothesStatusTable> table_change_clothes_status = FXCollections.observableArrayList();


    @FXML
    private TableView<ChangeClothesStatusTable> change_clothes_status_table;

    @FXML
    private TableColumn<ChangeClothesStatusTable, Integer> clothes_id;

    @FXML
    private TableColumn<ChangeClothesStatusTable, String> clothes_type;

    @FXML
    private TableColumn<ChangeClothesStatusTable, String> clothes_status;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        clothes_id.setCellValueFactory(new PropertyValueFactory<ChangeClothesStatusTable, Integer>("clothesID"));
        clothes_type.setCellValueFactory(new PropertyValueFactory<ChangeClothesStatusTable, String>("clothesType"));
        clothes_status.setCellValueFactory(new PropertyValueFactory<ChangeClothesStatusTable, String>("changeStatus"));
        for (Map.Entry<Integer, Integer> clothes : change_clothes_status_table_data.entrySet()) {
            table_change_clothes_status.add(new ChangeClothesStatusTable(clothes.getKey(), ClothesTypes.typeIntToString(clothes.getValue())));
        }
        change_clothes_status_table.setItems(table_change_clothes_status);
    }

    public void clothesCleaned(ActionEvent actionEvent) {
        ObservableList<ChangeClothesStatusTable> cleaned_clothes = FXCollections.observableArrayList();
        for (ChangeClothesStatusTable clean : table_change_clothes_status) {
            if (clean.getChangeStatus().isSelected()) {
                cleaned_clothes.add(clean);
                Clothes.changeStatus(1, 0, clean.getClothesID());
            }
        }
        JOptionPane.showMessageDialog(null, "Clothes Statuses have been updated!");
        String path = Controller.getFXMLRoute("EmployeeMainPage");
        Class class_type = getClass();
        Controller.loadForcedFXML(path, class_type);

    }

    public void goBack(ActionEvent actionEvent) {
        String route = "EmployeeMainPage";
        Class class_type = getClass();
        Buttons.goBack(actionEvent, route, class_type);
    }
}
