import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

public class ClothesHistory implements Initializable {

    @FXML
    private TableView<ClothesHistoryTable> clothes_history_table;

    @FXML
    private TableColumn<ClothesHistoryTable, Integer> receipt_id;

    @FXML
    private TableColumn<ClothesHistoryTable, Integer> clothes_id;

    @FXML
    private TableColumn<ClothesHistoryTable, String> received_date;

    @FXML
    private TableColumn<ClothesHistoryTable, String> cleaned_date;

    @FXML
    private TableColumn<ClothesHistoryTable, String> picked_up_date;

    @FXML
    private TextField filter_by_receipt_id;

    private Map<List<String>, Map<List<String>, List<String>>> clothes_history_dates = Clothes.getClothesHistoryDates();
    private Map<List<Integer>, List<Integer>> clothes_history_ids = Clothes.getClothesHistoryIDS();

    private List<String> received_dates = new ArrayList<>();
    private List<String> cleaned_dates = new ArrayList<>();
    private List<String> picked_up_dates = new ArrayList<>();
    private List<Integer> receipts_ids = new ArrayList<>();
    private List<Integer> clothes_ids = new ArrayList<>();

    private ObservableList<ClothesHistoryTable> clothes_history_table_data = FXCollections.observableArrayList();


    public void goBack(ActionEvent actionEvent) {
        String route = "Reports";
        Class class_type = getClass();
        Buttons.goBack(actionEvent, route, class_type);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        receipt_id.setCellValueFactory(new PropertyValueFactory<ClothesHistoryTable, Integer>("receiptID"));
        clothes_id.setCellValueFactory(new PropertyValueFactory<ClothesHistoryTable, Integer>("clothesID"));
        received_date.setCellValueFactory(new PropertyValueFactory<ClothesHistoryTable, String>("receivedDate"));
        cleaned_date.setCellValueFactory(new PropertyValueFactory<ClothesHistoryTable, String>("cleanedDate"));
        picked_up_date.setCellValueFactory(new PropertyValueFactory<ClothesHistoryTable, String>("pickedUpDate"));

        for (Map.Entry<List<String>, Map<List<String>, List<String>>> dates : clothes_history_dates.entrySet()) {
            Map<List<String>, List<String>> receipt_dates = dates.getValue();
            for (Map.Entry<List<String>, List<String>> receipt_dates_entries : receipt_dates.entrySet()) {
                received_dates = receipt_dates_entries.getKey();
                picked_up_dates = receipt_dates_entries.getValue();
            }
            cleaned_dates = dates.getKey();
        }

        for (Map.Entry<List<Integer>, List<Integer>> ids : clothes_history_ids.entrySet()) {
            receipts_ids = ids.getKey();
            clothes_ids = ids.getValue();
        }

        for (int i = 0; i < received_dates.size(); i++) {
            clothes_history_table_data.add(new ClothesHistoryTable(receipts_ids.get(i), clothes_ids.get(i), received_dates.get(i), cleaned_dates.get(i), picked_up_dates.get(i)));
        }

        clothes_history_table.setItems(clothes_history_table_data);
    }

    public void FilterDataByReceiptID(KeyEvent keyEvent) {
        FilteredList<ClothesHistoryTable> filtered_data = new FilteredList<ClothesHistoryTable>(clothes_history_table_data, p -> true);

        filter_by_receipt_id.textProperty().addListener((observable, old_value, new_value) -> {
            filtered_data.setPredicate(ClothesHistoryTable ->
            {
                if (new_value == null || new_value.isEmpty()) {
                    return true;
                }

//                String lowercase_filter = new_value.toLowerCase();

                //ADD MORE CHECKS IF NEEDED
                if (String.valueOf(ClothesHistoryTable.getReceiptID()).contains(new_value)) {
                    return true;
                }

                return false;
            });
        });

        SortedList<ClothesHistoryTable> sorted_data = new SortedList<>(filtered_data);

        sorted_data.comparatorProperty().bind(clothes_history_table.comparatorProperty());

        clothes_history_table.setItems(sorted_data);
    }
}
