import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import javax.swing.*;
import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

public class EmployeeMainPage implements Initializable {

    @FXML
    private Label employee_name;

    private HashMap<Integer, Integer> change_clothes_status_table_data = Clothes.getClothesByStatus(0);

    private static String first_name = Users.getUserFirstName();
    private static String last_name = Users.getUserLastName();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        employee_name.setText("Welcome, " + last_name + " " + first_name);
    }

    public void addNewTypeOfClothes(ActionEvent actionEvent) {
        String path = Controller.getFXMLRoute("AddNewTypeOfClothes");
        Class class_type = getClass();
        Controller.loadForcedFXML(path, class_type);
    }

    public void changeClothesStatus(ActionEvent actionEvent) {
        if (change_clothes_status_table_data.size() == 0) {
            JOptionPane.showMessageDialog(null, "All registered clothes are clean!");
        } else {
            String path = Controller.getFXMLRoute("ChangeClothesStatus");
            Class class_type = getClass();
            Controller.loadForcedFXML(path, class_type);
        }
    }


    public void reports(ActionEvent actionEvent) {
        String path = Controller.getFXMLRoute("Reports");
        Class class_type = getClass();
        Controller.loadForcedFXML(path, class_type);
    }

    public void logout(ActionEvent actionEvent) {
        Class class_type = getClass();
        Buttons.logout(actionEvent, class_type);
    }
}
