import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import javax.swing.*;
import java.net.URL;
import java.util.*;

public class ShowClothesStatusTable implements Initializable {

    private List<Integer> clothes_statuses = new ArrayList<>();

    private List<Integer> clothes_numbers = new ArrayList<>();

    private List<Integer> clothes_type = new ArrayList<>();

    @FXML
    private TableView<PickUpClothesTable> pick_up_clothes_table;

    @FXML
    private TableColumn<PickUpClothesTable, String> clothes_types;

    @FXML
    private TableColumn<PickUpClothesTable, Integer> number_of_clothes;

    @FXML
    private TableColumn<PickUpClothesTable, Integer> status;

    public void pickUpClothes(ActionEvent actionEvent) {
        if (Clothes.readyToPickUp(Receipts.getReceiptID())) {
            Clothes.changeStatus(2, Receipts.getReceiptID(), 0);
            int input = JOptionPane.showOptionDialog(null, "Clothes successfully picked up!", "Success!", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
            if (input == JOptionPane.OK_OPTION || input == JOptionPane.CLOSED_OPTION) {
                String path = Controller.getFXMLRoute("ClientMainPage");
                Class class_type = getClass();
                Controller.loadForcedFXML(path, class_type);
            }
        }
    }

    public void goBack(ActionEvent actionEvent) {
        String route = "CheckClothesStatus";
        Class class_type = getClass();
        Buttons.goBack(actionEvent, route, class_type);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        clothes_types.setCellValueFactory(new PropertyValueFactory<PickUpClothesTable, String>("clothesTypes"));
        number_of_clothes.setCellValueFactory(new PropertyValueFactory<PickUpClothesTable, Integer>("numberOfClothes"));
        status.setCellValueFactory(new PropertyValueFactory<PickUpClothesTable, Integer>("status"));
        HashMap<Integer, Map<Integer, Integer>> clothes_on_receipt = new HashMap<>();
        clothes_on_receipt = Receipts.getClothesByReceiptID(Receipts.getReceiptID());
        ObservableList<PickUpClothesTable> show_clothes_table_data = FXCollections.observableArrayList();
        for (Map.Entry<Integer, Map<Integer, Integer>> clothes_to_show : clothes_on_receipt.entrySet()) {
            Map<Integer, Integer> number_of_clothes_status = new HashMap<>();
            number_of_clothes_status = clothes_to_show.getValue();
            for (Map.Entry<Integer, Integer> number_status_entry : number_of_clothes_status.entrySet()) {
                clothes_statuses.add(number_status_entry.getValue());
                clothes_numbers.add(number_status_entry.getKey());
            }
            clothes_type.add(clothes_to_show.getKey());
        }
        for (int i = 0; i < clothes_type.size(); i++) {
            show_clothes_table_data.add(new PickUpClothesTable(ClothesTypes.typeIntToString(clothes_type.get(i)), clothes_numbers.get(i), Clothes.statusType(clothes_statuses.get(i))));
        }
        pick_up_clothes_table.setItems(show_clothes_table_data);
    }
}
