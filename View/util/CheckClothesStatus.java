import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import javax.swing.*;

public class CheckClothesStatus {


    @FXML
    private TextField receipt_id;

    public void checkReceiptID(ActionEvent actionEvent) {
        boolean check = Receipts.checkID(Integer.parseInt(receipt_id.getText()));
        if (!check) {
            JOptionPane.showMessageDialog(null, "Receipt ID not found!");
        } else {
            Receipts.setReceiptID(Integer.parseInt(receipt_id.getText()));
            if (Clothes.alreadyPickedUp(Receipts.getReceiptID())) {
                JOptionPane.showMessageDialog(null, "The clothes on this recipe have already been picked up!");
            } else {
                String path = Controller.getFXMLRoute("ShowClothesStatusTable");
                Class class_type = getClass();
                Controller.loadForcedFXML(path, class_type);
            }
        }
    }

    public void goBack(ActionEvent actionEvent) {
        String route = "ClientMainPage";
        Class class_type = getClass();
        Buttons.goBack(actionEvent, route, class_type);
    }
}
