import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

public class ReportsByClient implements Initializable {

    @FXML
    private TableView<ReportsByClientTable> reports_by_client_table;

    @FXML
    private TableColumn<ReportsByClientTable, String> first_and_last_name;

    @FXML
    private TableColumn<ReportsByClientTable, Integer> receipt_id;

    @FXML
    private TableColumn<ReportsByClientTable, Integer> number_of_clothes;

    @FXML
    private TableColumn<ReportsByClientTable, Integer> amount_payed;

    @FXML
    private TextField filter_by_first_and_last_name;


    private Map<List<String>, Map<List<Integer>, Map<List<Integer>, List<Integer>>>> reports_by_client = Clothes.getReportsByClientData();

    private List<String> client_name_to_table = new ArrayList<>();
    private List<Integer> receipt_id_to_table = new ArrayList<>();
    private List<Integer> clothes_number_to_table = new ArrayList<>();
    private List<Integer> amount_payed_to_table = new ArrayList<>();

    private ObservableList<ReportsByClientTable> reports_by_client_data = FXCollections.observableArrayList();

    public void filterByFirstAndLastName(KeyEvent keyEvent) {
        FilteredList<ReportsByClientTable> filtered_data = new FilteredList<ReportsByClientTable>(reports_by_client_data, p -> true);

        filter_by_first_and_last_name.textProperty().addListener((observable, old_value, new_value) -> {
            filtered_data.setPredicate(ReportsByClientTable ->
            {
                if (new_value == null || new_value.isEmpty()) {
                    return true;
                }

                String lowercase_filter = new_value.toLowerCase();

                //ADD MORE CHECKS IF NEEDED
                if (String.valueOf(ReportsByClientTable.getFirstAndLastName()).toLowerCase().contains(lowercase_filter)) {
                    return true;
                }

                return false;
            });
        });

        SortedList<ReportsByClientTable> sorted_data = new SortedList<>(filtered_data);

        sorted_data.comparatorProperty().bind(reports_by_client_table.comparatorProperty());

        reports_by_client_table.setItems(sorted_data);
    }

    public void goBack(ActionEvent actionEvent) {
        String route = "Reports";
        Class class_type = getClass();
        Buttons.goBack(actionEvent, route, class_type);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        first_and_last_name.setCellValueFactory(new PropertyValueFactory<ReportsByClientTable, String>("firstAndLastName"));
        receipt_id.setCellValueFactory(new PropertyValueFactory<ReportsByClientTable, Integer>("receiptID"));
        number_of_clothes.setCellValueFactory(new PropertyValueFactory<ReportsByClientTable, Integer>("numberOfClothes"));
        amount_payed.setCellValueFactory(new PropertyValueFactory<ReportsByClientTable, Integer>("amountPayed"));

        for (Map.Entry<List<String>, Map<List<Integer>, Map<List<Integer>, List<Integer>>>> names_receipts_clothes_payed : reports_by_client.entrySet()) {
            Map<List<Integer>, Map<List<Integer>, List<Integer>>> receipts_clothes_payed = names_receipts_clothes_payed.getValue();
            for (Map.Entry<List<Integer>, Map<List<Integer>, List<Integer>>> clothes_payed : receipts_clothes_payed.entrySet()) {
                Map<List<Integer>, List<Integer>> clothes_number_amount_payed = clothes_payed.getValue();
                for (Map.Entry<List<Integer>, List<Integer>> cleaned_cash : clothes_number_amount_payed.entrySet()) {
                    clothes_number_to_table = cleaned_cash.getKey();
                    amount_payed_to_table = cleaned_cash.getValue();
                }
                receipt_id_to_table = clothes_payed.getKey();
            }
            client_name_to_table = names_receipts_clothes_payed.getKey();
        }

        for (int i = 0; i < client_name_to_table.size(); i++) {
            reports_by_client_data.add(new ReportsByClientTable(client_name_to_table.get(i), receipt_id_to_table.get(i), clothes_number_to_table.get(i), amount_payed_to_table.get(i)));
        }
        reports_by_client_table.setItems((reports_by_client_data));
    }
}
