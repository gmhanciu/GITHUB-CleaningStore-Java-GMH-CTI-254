import javafx.event.ActionEvent;

public class Reports {

    public void reportsPerMonth(ActionEvent actionEvent) {
        String path = Controller.getFXMLRoute("ReportsPerMonth");
        Class class_type = getClass();
        Controller.loadForcedFXML(path, class_type);
    }

    public void clothesHistory(ActionEvent actionEvent) {
        String path = Controller.getFXMLRoute("ClothesHistory");
        Class class_type = getClass();
        Controller.loadForcedFXML(path, class_type);
    }

    public void reportsByClient(ActionEvent actionEvent) {
        String path = Controller.getFXMLRoute("ReportsByClient");
        Class class_type = getClass();
        Controller.loadForcedFXML(path, class_type);
    }

    public void goBack(ActionEvent actionEvent) {
        String route = "EmployeeMainPage";
        Class class_type = getClass();
        Buttons.goBack(actionEvent, route, class_type);
    }
}
