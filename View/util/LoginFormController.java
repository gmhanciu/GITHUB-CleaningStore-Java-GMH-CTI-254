import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import javax.swing.*;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;


public class LoginFormController implements Initializable {

    private static Parent rootParent;
    private static Stage mainStage = MainPage.getMainStage();

    @FXML
    private TextField username;

    @FXML
    private PasswordField password;

    private static Connection connection = Controller.getConnection();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if (Controller.getConnection() == null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public void validateLogin(ActionEvent actionEvent) {
        try {
            String user = username.getText();
            String pass = password.getText();
            if (user.isEmpty() || pass.isEmpty()) {
                JOptionPane.showMessageDialog(null, "Please fill in all the login fields!");
            } else {
                PreparedStatement validate_login = null;
                pass = CreateAccountFormController.encryptPassword(pass);
                String sql = "SELECT id, username, password, access, first_name, last_name FROM users where username = ? AND password = ?";
                validate_login = connection.prepareStatement(sql);
                validate_login.setString(1, user);
                validate_login.setString(2, pass);
                ResultSet rs = validate_login.executeQuery();
                if (rs.next()) {
                    Users.setUserName(rs.getString("username"));
                    Users.setUserAccess(Integer.parseInt(rs.getString("access")));
                    Users.setUserFirstName(rs.getString("first_name"));
                    Users.setUserLastName(rs.getString("last_name"));
                    Users.setUserID(rs.getInt("id"));
                    String path = null;
                    switch (Users.getUserAccess()) {
                        case "Employee":
                            path = Controller.getFXMLRoute("EmployeeMainPage");
                            break;
                        case "Client":
                            path = Controller.getFXMLRoute("ClientMainPage");
                            break;
                    }
                    Class class_type = getClass();
                    Controller.loadForcedFXML(path, class_type);

                } else {
                    JOptionPane.showMessageDialog(null, "Incorrect username or password! \n          Please try again!");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void loadCreateAccountForm(ActionEvent actionEvent) {


        try {
            String path = Controller.getFXMLRoute("CreateAccountForm");
            Class class_type = getClass();
            Controller.loadForcedFXML(path, class_type);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
