import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javax.swing.*;



public class MainPage extends Application {

    private static Parent rootParent;
    private static Stage mainStage;


    public static Parent getRootParent() {
        return rootParent;
    }

    public static void setRootParent(Parent rootParent) {
        MainPage.rootParent = rootParent;
    }

    public static Stage getMainStage() {
        return mainStage;
    }

    public static void setMainStage(Stage mainStage) {
        MainPage.mainStage = mainStage;
    }


    @Override
    public void start(Stage primaryStage) throws Exception {
        mainStage = primaryStage;
        try {
            Controller connection = new Controller();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "            Could not connect to the database! \nPlease make sure the server is up and try again!");
        }
        if (Controller.getConnection() != null) {
            try {
                rootParent = FXMLLoader.load(getClass().getResource("util/LoginForm.fxml"));
                Scene mainScene = new Scene(rootParent, 600, 400);
                mainStage.setTitle("Cleaning Store - Java - GMH - 254");
                mainStage.setScene(mainScene);
                mainStage.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.exit(1);
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
