import javax.swing.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Clothes {

    private static Connection connection = Controller.getConnection();


    public static void create(int type_id, int number_of_clothes, int receipt_id, String cleaned_date) {
        try {
            PreparedStatement create_clothes = null;
            String sql = "INSERT INTO clothes (type_id, number_of_clothes, receipt_id) VALUES (?, ?, ?)";
            create_clothes = connection.prepareStatement(sql);
            create_clothes.setInt(1, type_id);
            create_clothes.setInt(2, number_of_clothes);
            create_clothes.setInt(3, receipt_id);
            create_clothes.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Map<List<String>, Map<List<Integer>, Map<List<Integer>, List<Integer>>>> getReportsByClientData() {
        Map<List<Integer>, List<Integer>> clothes_number_total_cash = new HashMap<>();
        Map<List<Integer>, Map<List<Integer>, List<Integer>>> receipt_clothes_number_total_cash = new HashMap<>();
        Map<List<String>, Map<List<Integer>, Map<List<Integer>, List<Integer>>>> reports_by_client_data = new HashMap<>();

        List<String> client_name = new ArrayList<>();
        List<Integer> receipt_id = new ArrayList<>();
        List<Integer> clothes_number = new ArrayList<>();
        List<Integer> amount_payed = new ArrayList<>();

        try {
            PreparedStatement data = null;
            String sql = "SELECT users.first_name, users.last_name, receipts.id, SUM(clothes.number_of_clothes), SUM(clothes.number_of_clothes) * clothes_types.price FROM users, receipts, clothes, clothes_types WHERE receipts.user_id = users.id AND clothes.receipt_id = receipts.id AND clothes_types.id = clothes.type_id GROUP BY receipts.id";
            data = connection.prepareStatement(sql);
            ResultSet rs = data.executeQuery();
            while (rs.next()) {
                StringJoiner full_name = new StringJoiner(" ");
                full_name.add(rs.getString("users.last_name")).add(rs.getString("users.first_name"));
                client_name.add(full_name.toString());
                receipt_id.add(rs.getInt("receipts.id"));
                clothes_number.add(rs.getInt("SUM(clothes.number_of_clothes)"));
                amount_payed.add(rs.getInt("SUM(clothes.number_of_clothes) * clothes_types.price"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        clothes_number_total_cash.put(clothes_number, amount_payed);
        receipt_clothes_number_total_cash.put(receipt_id, clothes_number_total_cash);
        reports_by_client_data.put(client_name, receipt_clothes_number_total_cash);
        return reports_by_client_data;
    }

    public static Map<List<String>, Map<List<Integer>, Map<List<Integer>, List<Integer>>>> getReportsPerMonthData() {
        Map<List<Integer>, List<Integer>> clothes_number_total_cash = new HashMap<>();
        Map<List<Integer>, Map<List<Integer>, List<Integer>>> price_clothes_number_total_cash = new HashMap<>();
        Map<List<String>, Map<List<Integer>, Map<List<Integer>, List<Integer>>>> reports_per_month_data = new HashMap<>();

        List<String> clothes_types = new ArrayList<>();
        List<Integer> prices = new ArrayList<>();
        List<Integer> cleaned_clothes_number = new ArrayList<>();
        List<Integer> total_cash_earned = new ArrayList<>();

        try {
            PreparedStatement data = null;
            String sql = "SELECT clothes_types.name, clothes_types.price, SUM(clothes.number_of_clothes) FROM clothes_types, clothes WHERE clothes.type_id = clothes_types.id GROUP BY clothes_types.id";
            data = connection.prepareStatement(sql);
            ResultSet rs = data.executeQuery();
            while (rs.next()) {
                clothes_types.add(rs.getString("clothes_types.name"));
                prices.add(rs.getInt("clothes_types.price"));
                cleaned_clothes_number.add(rs.getInt("SUM(clothes.number_of_clothes)"));
                total_cash_earned.add(rs.getInt("SUM(clothes.number_of_clothes)") * rs.getInt("clothes_types.price"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        clothes_number_total_cash.put(cleaned_clothes_number, total_cash_earned);
        price_clothes_number_total_cash.put(prices, clothes_number_total_cash);
        reports_per_month_data.put(clothes_types, price_clothes_number_total_cash);
        return reports_per_month_data;
    }

    public static Map<List<String>, Map<List<String>, List<String>>> getClothesHistoryDates() {
        Map<List<String>, Map<List<String>, List<String>>> clothes_history_dates = new HashMap<>();
        Map<List<String>, List<String>> receipts_received_picked_up_dates = new HashMap<>();

        List<String> received_dates = new ArrayList<>();
        List<String> cleaned_dates = new ArrayList<>();
        List<String> picked_up_dates = new ArrayList<>();

        try {
            PreparedStatement dates = null;
            String sql = "SELECT receive_date, pick_up_date, clothes.cleaned_date FROM receipts, clothes WHERE clothes.receipt_id = receipts.id";
            dates = connection.prepareStatement(sql);
            ResultSet rs = dates.executeQuery();
            while (rs.next()) {
                received_dates.add(rs.getString("receive_date"));
                cleaned_dates.add(rs.getString("clothes.cleaned_date"));
                picked_up_dates.add(rs.getString("pick_up_date"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        receipts_received_picked_up_dates.put(received_dates, picked_up_dates);
        clothes_history_dates.put(cleaned_dates, receipts_received_picked_up_dates);
        return clothes_history_dates;
    }

    public static Map<List<Integer>, List<Integer>> getClothesHistoryIDS() {
        Map<List<Integer>, List<Integer>> clothes_history_ids = new HashMap<>();
        List<Integer> receipts_ids = new ArrayList<>();
        List<Integer> clothes_ids = new ArrayList<>();
        try {
            PreparedStatement ids = null;
            String sql = "SELECT receipts.id, clothes.id FROM receipts, clothes WHERE clothes.receipt_id = receipts.id";
            ids = connection.prepareStatement(sql);
            ResultSet rs = ids.executeQuery();
            int last_receipt_id = 0;
            while (rs.next()) {
                receipts_ids.add(rs.getInt("receipts.id"));
                clothes_ids.add(rs.getInt("clothes.id"));
            }
            clothes_history_ids.put(receipts_ids, clothes_ids);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return clothes_history_ids;
    }

    public static boolean readyToPickUp(int receipt_id) {
        try {
            PreparedStatement ready_to_pick_up = null;
            String sql = "SELECT status FROM clothes where receipt_id = ?";
            ready_to_pick_up = connection.prepareStatement(sql);
            ready_to_pick_up.setInt(1, receipt_id);
            ResultSet rs = ready_to_pick_up.executeQuery();
            while (rs.next()) {
                if (rs.getInt("status") != 1) {
                    JOptionPane.showMessageDialog(null, "Not all clothes are ready to be picked up!");
                    return false;
                }
            }
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean alreadyPickedUp(int receipt_id) {
        try {
            PreparedStatement already_picked_up = null;
            String sql = "SELECT status FROM clothes where receipt_id = ?";
            already_picked_up = connection.prepareStatement(sql);
            already_picked_up.setInt(1, receipt_id);
            ResultSet rs = already_picked_up.executeQuery();
            while (rs.next()) {
                if (rs.getInt("status") == 2) {
                    return true;
                }
            }
            return false;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }


    public static void changeStatus(int status, int receipt_id, int clothes_id) {
        try {
            PreparedStatement change_status = null;
            String sql = null;
            if (receipt_id == 0) {
                sql = "UPDATE clothes SET status = ? WHERE id = ?";
            } else if (clothes_id == 0) {
                sql = "UPDATE clothes SET status = ? WHERE receipt_id = ?";
            }
            change_status = connection.prepareStatement(sql);
            change_status.setInt(1, status);
            if (receipt_id == 0) {
                change_status.setInt(2, clothes_id);
            } else if (clothes_id == 0) {
                change_status.setInt(2, receipt_id);
            }
            change_status.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        String timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());

        try {
            PreparedStatement add_timestamps = null;
            String sql = null;
            if (receipt_id == 0) {
                sql = "UPDATE clothes SET cleaned_date = ? WHERE id = ?";
            } else if (clothes_id == 0) {
                sql = "UPDATE receipts SET pick_up_date = ? WHERE id = ?";
            }
            add_timestamps = connection.prepareStatement(sql);
            add_timestamps.setString(1, timestamp);
            if (receipt_id == 0) {
                add_timestamps.setInt(2, clothes_id);
            } else if (clothes_id == 0) {
                add_timestamps.setInt(2, receipt_id);
            }
            add_timestamps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static HashMap<Integer, Integer> getClothesByStatus(int status) {
        HashMap<Integer, Integer> clothes = new HashMap<>();
        try {
            PreparedStatement clothes_by_status = null;
            String sql = "SELECT * FROM clothes WHERE status = ?";
            clothes_by_status = connection.prepareStatement(sql);
            clothes_by_status.setInt(1, status);
            ResultSet rs = clothes_by_status.executeQuery();
            while (rs.next()) {
                if (rs.getInt("status") == 0) {
                    clothes.put(rs.getInt("id"), rs.getInt("type_id"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return clothes;
    }


    public static String statusType(int status_type) {
        switch (status_type) {
            case 0:
                return "Received";
            case 1:
                return "Cleaned";
            case 2:
                return "Picked Up";
        }
        return "Null";
    }
}
