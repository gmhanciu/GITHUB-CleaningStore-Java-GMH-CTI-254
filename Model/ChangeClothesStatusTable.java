import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.CheckBox;

public class ChangeClothesStatusTable {

    private final SimpleIntegerProperty clothesID;
    private final SimpleStringProperty clothesType;
    private CheckBox changeStatus;

    public ChangeClothesStatusTable(int clothesID, String clothesType) {
        this.clothesID = new SimpleIntegerProperty(clothesID);
        this.clothesType = new SimpleStringProperty(clothesType);
        this.changeStatus = new CheckBox();
    }

    public int getClothesID() {
        return clothesID.get();
    }

    public SimpleIntegerProperty clothesIDProperty() {
        return clothesID;
    }

    public void setClothesID(int clothesID) {
        this.clothesID.set(clothesID);
    }

    public String getClothesType() {
        return clothesType.get();
    }

    public SimpleStringProperty clothesTypeProperty() {
        return clothesType;
    }

    public void setClothesType(String clothesType) {
        this.clothesType.set(clothesType);
    }

    public CheckBox getChangeStatus() {
        return changeStatus;
    }

    public void setChangeStatus(CheckBox changeStatus) {
        this.changeStatus = changeStatus;
    }
}
