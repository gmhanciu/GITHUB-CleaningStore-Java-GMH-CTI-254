import javax.swing.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class Receipts {

    private static Connection connection = Controller.getConnection();

    private static int receipt_id;

    public static int getReceiptID() {
        return receipt_id;
    }

    public static void setReceiptID(int receipt_id) {
        Receipts.receipt_id = receipt_id;
    }

    public static void create(int user_id, int total_price, Map<String, Integer> clothes_data) {
        String received_date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());

        if (total_price == 0) {
            JOptionPane.showMessageDialog(null, "Please add a number for a clothes type!");
        } else {
            try {
                int type_id = 0;
                int number_of_clothes = 0;
                PreparedStatement create_receipt = null;
                String sql = "INSERT INTO receipts (user_id, receive_date, price) VALUES (?, ?, ?)";
                create_receipt = connection.prepareStatement(sql);
                create_receipt.setInt(1, user_id);
                create_receipt.setString(2, received_date);
                create_receipt.setInt(3, total_price);
                int check_insert = create_receipt.executeUpdate();
                if (check_insert != 0) {
                    int receipt_id = Receipts.getID(user_id, received_date, total_price);
                    int input = JOptionPane.showOptionDialog(null, "Clothes have been registered successfully on the receipt with ID: " + receipt_id + ". Please note down the ID so you can use it to check your clothes status!", "Success!", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
                    if (input == JOptionPane.OK_OPTION || input == JOptionPane.CLOSED_OPTION) {
                        for (Map.Entry<String, Integer> clothes_to_add : clothes_data.entrySet()) {
                            type_id = ClothesTypes.typeStringToInt(clothes_to_add.getKey());
                            number_of_clothes = clothes_to_add.getValue();

                            Clothes.create(type_id, number_of_clothes, receipt_id, received_date);
                        }
                        String path = Controller.getFXMLRoute("ClientMainPage");
                        Class class_type = Receipts.class;
                        Controller.loadForcedFXML(path, class_type);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Error encountered! Please try again adding clothes!");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public static int getID(int user_id, String received_date, int total_price) {
        try {
            PreparedStatement get_receipt_id = null;
            String sql = "SELECT id FROM receipts WHERE user_id = ? AND receive_date = ? AND price = ?";
            get_receipt_id = connection.prepareStatement(sql);
            get_receipt_id.setInt(1, user_id);
            get_receipt_id.setString(2, received_date);
            get_receipt_id.setInt(3, total_price);
            ResultSet rs = get_receipt_id.executeQuery();
            if (rs.next()) {
                return rs.getInt("id");
            } else {
                JOptionPane.showMessageDialog(null, "Error encountered while retrieving RECEIPT_ID");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static boolean checkID(int receipt_id_to_check) {
        try {
            PreparedStatement check_receipt_id = null;
            String sql = "SELECT id FROM receipts WHERE id = ?";
            check_receipt_id = connection.prepareStatement(sql);
            check_receipt_id.setInt(1, receipt_id_to_check);
            ResultSet rs = check_receipt_id.executeQuery();
            if (rs.next()) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static HashMap<Integer, Map<Integer, Integer>> getClothesByReceiptID(int receipt_id) {
        HashMap<Integer, Map<Integer, Integer>> clothes_on_receipt = new HashMap<>();
        try {
            PreparedStatement clothes_by_receipt_id = null;
            String sql = "SELECT type_id, number_of_clothes, status FROM clothes WHERE receipt_id = ?";
            clothes_by_receipt_id = connection.prepareStatement(sql);
            clothes_by_receipt_id.setInt(1, receipt_id);
            ResultSet rs = clothes_by_receipt_id.executeQuery();
            while (rs.next()) {
                Map<Integer, Integer> number_of_clothes_and_status = new HashMap<>();
                number_of_clothes_and_status.put(rs.getInt("number_of_clothes"), rs.getInt("status"));
                clothes_on_receipt.put(rs.getInt("type_id"), number_of_clothes_and_status);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return clothes_on_receipt;
    }
}
