import java.sql.Connection;

public class Users {

    private static String user_access;
    private static String user_name;
    private static String user_first_name;
    private static String user_last_name;
    private static int user_id;

    private static Connection connection = Controller.getConnection();

    public static int getUserID() {
        return user_id;
    }

    public static void setUserID(int user_id) {
        Users.user_id = user_id;
    }

    public static String getUserAccess() {
        return user_access;
    }

    public static void setUserAccess(int user_access) {
        switch (user_access) {
            case 1:
                Users.user_access = "Employee";
                break;
            case 2:
                Users.user_access = "Client";
                break;
        }

    }

    public static String getUserName() {
        return user_name;
    }

    public static void setUserName(String user_name) {
        Users.user_name = user_name;
    }

    public static String getUserFirstName() {
        return user_first_name;
    }

    public static void setUserFirstName(String user_first_name) {
        Users.user_first_name = user_first_name;
    }

    public static String getUserLastName() {
        return user_last_name;
    }

    public static void setUserLastName(String user_last_name) {
        Users.user_last_name = user_last_name;
    }
}
