import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class PickUpClothesTable {

    private final SimpleStringProperty clothesTypes;
    private final SimpleIntegerProperty numberOfClothes;
    private final SimpleStringProperty status;

    public PickUpClothesTable(String clothes_types, int number_of_clothes, String status)
    {
        this.clothesTypes = new SimpleStringProperty(clothes_types);
        this.numberOfClothes = new SimpleIntegerProperty(number_of_clothes);
        this.status = new SimpleStringProperty(status);
    }

    public String getClothesTypes() {
        return clothesTypes.get();
    }

    public SimpleStringProperty clothesTypesProperty() {
        return clothesTypes;
    }

    public void setClothesTypes(String clothesTypes) {
        this.clothesTypes.set(clothesTypes);
    }

    public int getNumberOfClothes() {
        return numberOfClothes.get();
    }

    public SimpleIntegerProperty numberOfClothesProperty() {
        return numberOfClothes;
    }

    public void setNumberOfClothes(int numberOfClothes) {
        this.numberOfClothes.set(numberOfClothes);
    }

    public String getStatus() {
        return status.get();
    }

    public SimpleStringProperty statusProperty() {
        return status;
    }

    public void setStatus(String status) {
        this.status.set(status);
    }
}
