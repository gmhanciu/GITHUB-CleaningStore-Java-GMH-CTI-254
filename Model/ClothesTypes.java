import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClothesTypes {

    private static Connection connection = Controller.getConnection();


    public static String[] getAllTypes() {
        List<String> all_types = new ArrayList<>();
        try {
            PreparedStatement get_all_types_of_clothes = null;
            String sql = "SELECT name FROM clothes_types";
            get_all_types_of_clothes = connection.prepareStatement(sql);
            ResultSet rs = get_all_types_of_clothes.executeQuery();
            while (rs.next()) {
                all_types.add(rs.getString("name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return all_types.toArray(new String[0]);
    }

    public static Integer[] getAllPrices() {
        List<Integer> all_prices = new ArrayList<>();
        try {
            PreparedStatement get_all_prices_of_types = null;
            String sql = "SELECT price FROM clothes_types";
            get_all_prices_of_types = connection.prepareStatement(sql);
            ResultSet rs = get_all_prices_of_types.executeQuery();
            while (rs.next()) {
                all_prices.add(rs.getInt("price"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return all_prices.toArray(new Integer[0]);
    }

    public static void insertType(String type_name, int price) {
        try {
            PreparedStatement insert_new_type_of_clothes = null;
            String sql = "INSERT INTO clothes_types (name, price) VALUES (?, ?)";
            insert_new_type_of_clothes = connection.prepareStatement(sql);
            insert_new_type_of_clothes.setString(1, type_name);
            insert_new_type_of_clothes.setInt(2, price);
            int check_insert = insert_new_type_of_clothes.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static int typeStringToInt(String clothes_type_name) {
        try {
            PreparedStatement type_string_to_int = null;
            String sql = "SELECT id FROM clothes_types where name = ?";
            type_string_to_int = connection.prepareStatement(sql);
            type_string_to_int.setString(1, clothes_type_name);
            ResultSet rs = type_string_to_int.executeQuery();
            if (rs.next()) {
                return rs.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return 0;
    }

    public static String typeIntToString(int clothes_type) {
        try {
            PreparedStatement type_int_to_string = null;
            String sql = "SELECT name FROM clothes_types where id = ?";
            type_int_to_string = connection.prepareStatement(sql);
            type_int_to_string.setInt(1, clothes_type);
            ResultSet rs = type_int_to_string.executeQuery();
            if (rs.next()) {
                return rs.getString("name");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "Null";
    }
}
