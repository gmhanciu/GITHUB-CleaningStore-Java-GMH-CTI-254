import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class AddClothesTable {

    private final SimpleStringProperty clothesTypes;
    private final SimpleIntegerProperty numberOfClothes;
    private final SimpleIntegerProperty price;

    public AddClothesTable(String clothes_types, int number_of_clothes, int price) {
        this.clothesTypes = new SimpleStringProperty(clothes_types);
        this.numberOfClothes = new SimpleIntegerProperty(number_of_clothes);
        this.price = new SimpleIntegerProperty(price);
    }

    public String getClothesTypes() {
        return clothesTypes.get();
    }

    public SimpleStringProperty clothesTypesProperty() {
        return clothesTypes;
    }

    public void setClothesTypes(String clothesTypes) {
        this.clothesTypes.set(clothesTypes);
    }

    public int getNumberOfClothes() {
        return numberOfClothes.get();
    }

    public SimpleIntegerProperty numberOfClothesProperty() {
        return numberOfClothes;
    }

    public void setNumberOfClothes(int numberOfClothes) {
        this.numberOfClothes.set(numberOfClothes);
    }

    public int getPrice() {
        return price.get();
    }

    public SimpleIntegerProperty priceProperty() {
        return price;
    }

    public void setPrice(int price) {
        this.price.set(price);
    }
}
