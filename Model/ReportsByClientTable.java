import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class ReportsByClientTable {

    private final SimpleStringProperty firstAndLastName;
    private final SimpleIntegerProperty receiptID;
    private final SimpleIntegerProperty numberOfClothes;
    private final SimpleIntegerProperty amountPayed;

    public ReportsByClientTable(String firstAndLastName, int receiptID, int numberOfClothes, int amountPayed) {
        this.firstAndLastName = new SimpleStringProperty(firstAndLastName);
        this.receiptID = new SimpleIntegerProperty(receiptID);
        this.numberOfClothes = new SimpleIntegerProperty(numberOfClothes);
        this.amountPayed = new SimpleIntegerProperty(amountPayed);
    }

    public String getFirstAndLastName() {
        return firstAndLastName.get();
    }

    public SimpleStringProperty firstAndLastNameProperty() {
        return firstAndLastName;
    }

    public void setFirstAndLastName(String firstAndLastName) {
        this.firstAndLastName.set(firstAndLastName);
    }

    public int getReceiptID() {
        return receiptID.get();
    }

    public SimpleIntegerProperty receiptIDProperty() {
        return receiptID;
    }

    public void setReceiptID(int receiptID) {
        this.receiptID.set(receiptID);
    }

    public int getNumberOfClothes() {
        return numberOfClothes.get();
    }

    public SimpleIntegerProperty numberOfClothesProperty() {
        return numberOfClothes;
    }

    public void setNumberOfClothes(int numberOfClothes) {
        this.numberOfClothes.set(numberOfClothes);
    }

    public int getAountPayed() {
        return amountPayed.get();
    }

    public SimpleIntegerProperty amountPayedProperty() {
        return amountPayed;
    }

    public void setAmountPayed(int amountPayed) {
        this.amountPayed.set(amountPayed);
    }
}
