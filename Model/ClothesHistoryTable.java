import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class ClothesHistoryTable {

    private final SimpleIntegerProperty receiptID;
    private final SimpleIntegerProperty clothesID;
    private final SimpleStringProperty receivedDate;
    private final SimpleStringProperty cleanedDate;
    private final SimpleStringProperty pickedUpDate;

    public ClothesHistoryTable(int receiptID, int clothesID, String receivedDate, String cleanedDate, String pickedUpDate) {
        this.receiptID = new SimpleIntegerProperty(receiptID);
        this.clothesID = new SimpleIntegerProperty(clothesID);
        this.receivedDate = new SimpleStringProperty(receivedDate);
        this.cleanedDate = new SimpleStringProperty(cleanedDate);
        this.pickedUpDate = new SimpleStringProperty(pickedUpDate);
    }

    public int getReceiptID() {
        return receiptID.get();
    }

    public SimpleIntegerProperty receiptIDProperty() {
        return receiptID;
    }

    public void setReceiptID(int receiptID) {
        this.receiptID.set(receiptID);
    }

    public int getClothesID() {
        return clothesID.get();
    }

    public SimpleIntegerProperty clothesIDProperty() {
        return clothesID;
    }

    public void setClothesID(int clothesID) {
        this.clothesID.set(clothesID);
    }

    public String getReceivedDate() {
        return receivedDate.get();
    }

    public SimpleStringProperty receivedDateProperty() {
        return receivedDate;
    }

    public void setReceivedDate(String receivedDate) {
        this.receivedDate.set(receivedDate);
    }

    public String getCleanedDate() {
        return cleanedDate.get();
    }

    public SimpleStringProperty cleanedDateProperty() {
        return cleanedDate;
    }

    public void setCleanedDate(String cleanedDate) {
        this.cleanedDate.set(cleanedDate);
    }

    public String getPickedUpDate() {
        return pickedUpDate.get();
    }

    public SimpleStringProperty pickedUpDateProperty() {
        return pickedUpDate;
    }

    public void setPickedUpDate(String pickedUpDate) {
        this.pickedUpDate.set(pickedUpDate);
    }
}
