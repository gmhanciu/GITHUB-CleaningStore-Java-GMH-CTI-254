import javafx.event.ActionEvent;

public class Buttons {

    public static void goBack(ActionEvent actionEvent, String route, Class class_type) {
        try {
            String path = Controller.getFXMLRoute(route);
            Controller.loadForcedFXML(path, class_type);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void logout(ActionEvent actionEvent, Class class_type) {
        String path = Controller.getFXMLRoute("LoginForm");
        Controller.loadForcedFXML(path, class_type);
    }
}
