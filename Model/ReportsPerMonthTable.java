import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class ReportsPerMonthTable {

    private final SimpleStringProperty clothesType;
    private final SimpleIntegerProperty price;
    private final SimpleIntegerProperty cleanedClothesNumber;
    private final SimpleIntegerProperty totalCashEarned;

    public ReportsPerMonthTable(String clothesType, int price, int cleanedClothesNumber, int totalCashEarned) {
        this.clothesType = new SimpleStringProperty(clothesType);
        this.price = new SimpleIntegerProperty(price);
        this.cleanedClothesNumber = new SimpleIntegerProperty(cleanedClothesNumber);
        this.totalCashEarned = new SimpleIntegerProperty(totalCashEarned);
    }

    public String getClothesType() {
        return clothesType.get();
    }

    public SimpleStringProperty clothesTypeProperty() {
        return clothesType;
    }

    public void setClothesType(String clothesType) {
        this.clothesType.set(clothesType);
    }

    public int getPrice() {
        return price.get();
    }

    public SimpleIntegerProperty priceProperty() {
        return price;
    }

    public void setPrice(int price) {
        this.price.set(price);
    }

    public int getCleanedClothesNumber() {
        return cleanedClothesNumber.get();
    }

    public SimpleIntegerProperty cleanedClothesNumberProperty() {
        return cleanedClothesNumber;
    }

    public void setCleanedClothesNumber(int cleanedClothesNumber) {
        this.cleanedClothesNumber.set(cleanedClothesNumber);
    }

    public int getTotalCashEarned() {
        return totalCashEarned.get();
    }

    public SimpleIntegerProperty totalCashEarnedProperty() {
        return totalCashEarned;
    }

    public void setTotalCashEarned(int totalCashEarned) {
        this.totalCashEarned.set(totalCashEarned);
    }
}
