public enum FXMLRoutes {

    LoginForm("LoginForm", "/LoginForm.fxml"),
    CreateAccountForm("CreateAccountForm", "/CreateAccountForm.fxml"),
    ClientMainPage("ClientMainPage", "/ClientMainPage.fxml"),
    AddClothes("AddClothes", "/AddClothes.fxml"),
    CheckClothesStatus("CheckClothesStatus", "/CheckClothesStatus.fxml"),
    ShowClothesStatusTable("ShowClothesStatusTable", "/ShowClothesStatusTable.fxml"),
    EmployeeMainPage("EmployeeMainPage", "/EmployeeMainPage.fxml"),
    AddNewTypeOfClothes("AddNewTypeOfClothes", "/AddNewTypeOfClothes.fxml"),
    ChangeClothesStatus("ChangeClothesStatus", "/ChangeClothesStatus.fxml"),
    Reports("Reports", "/Reports.fxml"),
    ReportsPerMonth("ReportsPerMonth", "/ReportsPerMonth.fxml"),
    ClothesHistory("ClothesHistory", "/ClothesHistory.fxml"),
    ReportsByClient("ReportsByClient", "/ReportsByClient.fxml");


    private String name;
    private String path;

    FXMLRoutes(String name, String path) {
        this.name = name;
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public String getName() {
        return name;
    }
}
