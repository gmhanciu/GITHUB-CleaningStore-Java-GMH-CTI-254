-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 14, 2018 at 10:15 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cleaning_store`
--

-- --------------------------------------------------------

--
-- Table structure for table `clothes`
--

CREATE TABLE `clothes` (
  `id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL COMMENT '1 = tricou / 2 = rochie',
  `number_of_clothes` int(11) NOT NULL,
  `receipt_id` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '0 = received / 1 = cleaned / 2 = picked up',
  `cleaned_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clothes`
--

INSERT INTO `clothes` (`id`, `type_id`, `number_of_clothes`, `receipt_id`, `status`, `cleaned_date`) VALUES
(34, 1, 5, 18, 2, '2018-05-13 22:13:56'),
(35, 4, 1, 18, 2, '2018-05-13 22:14:05'),
(36, 2, 2, 18, 2, '2018-05-13 22:14:01'),
(37, 3, 3, 18, 2, '2018-05-13 22:14:05'),
(38, 1, 5, 19, 2, '2018-05-13 22:14:01'),
(39, 4, 5, 19, 2, '2018-05-13 22:14:08'),
(40, 2, 1, 19, 2, '2018-05-13 22:14:01'),
(41, 3, 5, 19, 2, '2018-05-13 22:14:05'),
(42, 1, 12, 20, 2, '2018-05-14 23:08:07'),
(43, 4, 19, 20, 2, '2018-05-14 23:08:07'),
(44, 2, 15, 20, 2, '2018-05-14 23:08:07'),
(45, 3, 7, 20, 2, '2018-05-14 23:08:07');

-- --------------------------------------------------------

--
-- Table structure for table `clothes_types`
--

CREATE TABLE `clothes_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Services management (shirts, dresses, etc.)';

--
-- Dumping data for table `clothes_types`
--

INSERT INTO `clothes_types` (`id`, `name`, `price`) VALUES
(1, 'Tricou', 15),
(2, 'Rochie', 50),
(3, 'Perdea', 125),
(4, 'Sacou', 85);

-- --------------------------------------------------------

--
-- Table structure for table `receipts`
--

CREATE TABLE `receipts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `receive_date` datetime NOT NULL,
  `pick_up_date` datetime DEFAULT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `receipts`
--

INSERT INTO `receipts` (`id`, `user_id`, `receive_date`, `pick_up_date`, `price`) VALUES
(18, 9, '2018-05-13 22:13:36', '2018-05-13 22:14:28', 635),
(19, 9, '2018-05-13 22:13:46', '2018-05-13 22:14:31', 1175),
(20, 10, '2018-05-14 23:07:55', '2018-05-14 23:08:21', 3420);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `access` int(11) NOT NULL DEFAULT '2' COMMENT '1 = admin/employee ; 2 = client'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `first_name`, `last_name`, `access`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@example.org', 'Admin', 'Example', 1),
(9, 'client', '62608e08adc29a8d6dbc9754e659f125', 'client@example.org', 'Client', 'Example', 2),
(10, 'client2', '2c66045d4e4a90814ce9280272e510ec', 'client2@example.org', 'Example', 'Client2', 2),
(11, 'client3', 'c27af3f6460eb10979adb366fc7f6856', 'client3@example.org', 'Client3', 'Example', 2),
(12, 'client4', 'de285ec98e0f83211da217a4e1c5923e', 'client4@example.org', 'Client4', 'Example', 2),
(13, 'c5', '25ea1682e16466c0667abdc095920f6c', 'c5@example.org', 'c5', 'example', 2),
(14, 'c6', '5a34d1edaea4e32871b6f7503ad4727e', 'c6@example.org', 'c6', 'example', 2),
(15, 'c7', '4d3a21d8c684c09c19b93be911827fd5', 'c7@example.org', 'c7', 'example', 2),
(16, 'c8', '7cd1d2b54911b95b06b1c423bd551f2f', 'c8@example.org', 'c8', 'example', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clothes`
--
ALTER TABLE `clothes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clothes_types`
--
ALTER TABLE `clothes_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipts`
--
ALTER TABLE `receipts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clothes`
--
ALTER TABLE `clothes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `clothes_types`
--
ALTER TABLE `clothes_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `receipts`
--
ALTER TABLE `receipts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
