import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;

public class Controller {

    private static Connection connection = null;


    private static Parent rootParent;
    private static Stage mainStage = MainPage.getMainStage();

    public Controller() throws Exception {
        String host = "jdbc:mysql://localhost:3306/cleaning_store";
        String username = "root";
        String password = "";
        connection = DriverManager.getConnection(host, username, password);
    }

    public static Connection getConnection() {
        return connection;
    }

    public static boolean statusConnection() {
        return connection != null;
    }

    public static void loadForcedFXML(String full_path_fxml, Class class_type) {

        try {
            rootParent = FXMLLoader.load(class_type.getResource(full_path_fxml));
            Scene mainScene = new Scene(rootParent);
            mainStage.setScene(mainScene);
            mainStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getFXMLRoute(String name) {

        switch (name) {
            case "LoginForm":
                return FXMLRoutes.LoginForm.getPath();
            case "CreateAccountForm":
                return FXMLRoutes.CreateAccountForm.getPath();
            case "ClientMainPage":
                return FXMLRoutes.ClientMainPage.getPath();
            case "AddClothes":
                return FXMLRoutes.AddClothes.getPath();
            case "CheckClothesStatus":
                return FXMLRoutes.CheckClothesStatus.getPath();
            case "ShowClothesStatusTable":
                return FXMLRoutes.ShowClothesStatusTable.getPath();
            case "EmployeeMainPage":
                return FXMLRoutes.EmployeeMainPage.getPath();
            case "AddNewTypeOfClothes":
                return FXMLRoutes.AddNewTypeOfClothes.getPath();
            case "ChangeClothesStatus":
                return FXMLRoutes.ChangeClothesStatus.getPath();
            case "Reports":
                return FXMLRoutes.Reports.getPath();
            case "ReportsPerMonth":
                return FXMLRoutes.ReportsPerMonth.getPath();
            case "ClothesHistory":
                return FXMLRoutes.ClothesHistory.getPath();
            case "ReportsByClient":
                return FXMLRoutes.ReportsByClient.getPath();
        }
        return null;
    }

}
